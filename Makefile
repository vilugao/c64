# GNU Assembler
AS = as
ASFLAGS = -R
# Linker
LD = ld
LDFLAGS =
# Object dumper in Binutils package
OBJDUMP = objdump
# Pager utility
PAGER = less

SRCS = c64.S
TARGET_DOS = c64.com
TARGET_BOOT = c64.img

.PHONY: all
all: dos boot

.PHONY: dos
dos: $(TARGET_DOS)

.PHONY: boot
boot: $(TARGET_BOOT)

.PHONY: clean
clean:
	-$(RM) $(TARGET_DOS) $(TARGET_BOOT) $(SRCS:.S=.o)

.PHONY: dump
dump: dump-c64.o

.PHONY: dump-%
dump-%.com dump-%.img: OBJDUMPFLAGS = -m i8086 -b binary -D
dump-%.o: OBJDUMPFLAGS = -m i8086 -d
dump-%: %
	$(OBJDUMP) $(OBJDUMPFLAGS) $< | $(PAGER)

%.o: %.S
	$(AS) $(ASFLAGS) -o $@ $<

%.img: LDFLAGS = --oformat binary -Ttext 0x7c00
%.img: %.o
	$(LD) $(LDFLAGS) -o $@ $<
	-chmod -x $@
#	-truncate -s 737280 $@

%.com: LDFLAGS = --oformat binary -Ttext 0x0100
%.com: %.o
	$(LD) $(LDFLAGS) -o $@ $<
